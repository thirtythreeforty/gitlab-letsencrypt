FROM node:12-alpine
RUN apk add git

# Create app directory
WORKDIR /usr/src/app

# Bundle app source
COPY . .

RUN npm install --no-optional -g
# If you are building your code for production
# RUN npm ci --only=production

CMD [ "gitlab-le" ]
